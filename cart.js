const categoriesList = [
  { id: 1, name: "Categorie Name 1", check: false },
  { id: 2, name: "Categorie Name 2", check: false },
  { id: 3, name: "Categorie Name 3", check: false },
  { id: 4, name: "Categorie Name 4", check: false },
  { id: 5, name: "Categorie Name 5", check: false },
  { id: 6, name: "Categorie Name 6", check: false },
  { id: 7, name: "Categorie Name 7", check: false },
  { id: 8, name: "Categorie Name 8", check: false },
];

const colorsList = [
  { id: 1, name: "Color1", color: "red" },
  { id: 2, name: "Color2", color: "blue" },
  { id: 3, name: "Color3", color: "gray" },
  { id: 4, name: "Color4", color: "cyan" },
  { id: 5, name: "Color5", color: "bisque" },
  { id: 6, name: "Color6", color: "black" },
  { id: 7, name: "Color7", color: "green" },
  { id: 8, name: "Color8", color: "purple" },
];

let categories = document.querySelector(".dca-container");
let colors = document.querySelector(".dc-container");

categoriesList.forEach((value) => {
  let newDiv = document.createElement("div");
  newDiv.classList.add("diff-ca");
  newDiv.innerHTML = `
            <input type="checkbox" name="" id="${value.id}">
            <p>${value.name}</p>
        `;
  categories.appendChild(newDiv);
});

colorsList.forEach((el) => {
  let newDiv = document.createElement("div");
  newDiv.classList.add("diff-col");
  newDiv.innerHTML = `
      <span style="background-color:${el.color};" class="circle"></span>
      <p class="text" >${el.name}</p>
  `;
  colors.appendChild(newDiv);
});

const rangeInput = document.querySelectorAll(".range-input input"),
  priceInput = document.querySelectorAll(".price-input input"),
  range = document.querySelector(".slider .progress");
let priceGap = 1000;

priceInput.forEach((input) => {
  input.addEventListener("input", (e) => {
    let minPrice = parseInt(priceInput[0].value),
      maxPrice = parseInt(priceInput[1].value);

    if (maxPrice - minPrice >= priceGap && maxPrice <= rangeInput[1].max) {
      if (e.target.className === "input-min") {
        rangeInput[0].value = minPrice;
        range.style.left = (minPrice / rangeInput[0].max) * 100 + "%";
      } else {
        rangeInput[1].value = maxPrice;
        range.style.right = 100 - (maxPrice / rangeInput[1].max) * 100 + "%";
      }
    }
  });
});

rangeInput.forEach((input) => {
  input.addEventListener("input", (e) => {
    let minVal = parseInt(rangeInput[0].value);
    let maxVal = parseInt(rangeInput[1].value);

    if (maxVal - minVal < priceGap) {
      if (e.target.className === "range-min") {
        rangeInput[0].value = maxVal - priceGap;
      } else {
        rangeInput[1].value = minVal + priceGap;
      }
    } else {
      priceInput[0].value = minVal;
      priceInput[1].value = maxVal;
      range.style.left = (minVal / rangeInput[0].max) * 100 + "%";
      range.style.right = 100 - (maxVal / rangeInput[1].max) * 100 + "%";
    }
  });
});

const productList = [
  {
    id: 1,
    name: "Pantalon Bleu Femme",
    image: "images/IMG-1.jpg",
    price: 200,
    like: false,
  },
  {
    id: 2,
    name: "Gilet en Laine",
    image: "images/IMG-2.jpg",
    price: 5,
    like: false,
  },
  {
    id: 3,
    name: "Robe en Nilon",
    image: "images/IMG-3.jpg",
    price: 12,
    like: false,
  },
  {
    id: 4,
    name: "Robe en Coton",
    image: "images/IMG-4.jpg",
    price: 30,
    like: false,
  },
  {
    id: 5,
    name: "Jacket",
    image: "images/IMG-5.jpg",
    price: 21,
    like: false,
  },
  {
    id: 6,
    name: "Montre en argent",
    image: "images/IMG-6.jpg",
    price: 74,
    like: false,
  },
  {
    id: 7,
    name: "Class Outfit",
    image: "images/IMG-7.jpg",
    price: 94,
    like: false,
  },
  {
    id: 8,
    name: "Veste Tuivasa",
    image: "images/IMG-8.jpg",
    price: 849,
    like: false,
  },
  {
    id: 9,
    name: "Streetwear Outfit",
    image: "images/IMG-9.jpg",
    price: 2,
    like: false,
  },
  {
    id: 10,
    name: "Crocs Rose",
    image: "images/IMG-10.jpg",
    price: 1,
    like: false,
  },
  {
    id: 11,
    name: "Tunique Bébé",
    image: "images/IMG-11.jpg",
    price: 27,
    like: false,
  },
  {
    id: 12,
    name: "Veste Cartapola",
    image: "images/IMG-12.jpg",
    price: 747,
    like: false,
  },
  {
    id: 13,
    name: "Chapeau Mounene",
    image: "images/IMG-13.jpg",
    price: 984,
    like: false,
  },
];

const searchInput = document.getElementById("searchInput");
const searchResults = document.getElementById("searchResults");
searchResults.style.display = "none";

function displayResults(results) {
  searchResults.innerHTML = "";
  results.forEach((result) => {
    const li = document.createElement("li");
    if (results !== "") searchResults.style.display = "block";
    li.style.borderBottom = "1px solid #e8e9eb";
    li.style.padding = "1em";
    li.textContent = result.name;
    searchResults.appendChild(li);
  });
}

searchInput.addEventListener("input", () => {
  let results;
  const query = searchInput.value.toLowerCase().trim();
  if (query == "") {
    results = "";
  } else {
    results = productList.filter((item) =>
      item.name.toLowerCase().includes(query)
    );
  }
  displayResults(results);
});

let articles = document.querySelector(".articles");
productList.forEach((el) => {
  let myDiv = document.createElement("div");
  myDiv.classList.add("item", "overlay");
  myDiv.innerHTML = `
        <div class="essatials" id="${el.id}">
            <div class="img-item">
                <img src="images/like.svg" alt="" class="like-dislike" onclick="likeDislike(${el.id})">
                <img src="images/plus.svg" alt="" class="add-cart" onclick="addToCart(${el.id})">
                <img src="${el.image}" alt="" class="product-img">
            </div>
            <div class="item-info">
                <p class="name-item">${el.name}</p>
                <span class="price-item">$${el.price}</span>
            </div>
        </div>
    `;
  articles.appendChild(myDiv);
});

let like_dislike = document.querySelectorAll(".like-dislike");
function likeDislike(item) {
  let index = productList.findIndex((el) => el.id === item);
  productList[index].like = !productList[index].like;
  productList[index].like
    ? (like_dislike[index].style.filter = "none")
    : (like_dislike[index].style.filter = "grayscale(100%)");
}

var cart = [];

function addToCart(item) {
  let important = productList.filter((el) => el.id == item);
  let imp = cart.filter((el) => el.id == important[0].id);
  if (imp.length == 0) {
    cart.push({ ...important[0], quantity: 1 });
  }
  card();
}

function increase(item) {
  let important = cart.filter((el) => el.id == item);
  for (const obj of cart) {
    if (JSON.stringify(obj) === JSON.stringify(important[0])) {
      obj.quantity++;
      break;
    }
  }
  card();
}

function decrease(item) {
  let index = cart.findIndex((el) => el.id === item);
  if (index !== -1) {
    if (cart[index].quantity > 1) {
      cart[index].quantity--;
    } else {
      cart.splice(index, 1);
    }
  }
  card();
}

let clearCartButton = document.querySelector("#clearCartButton");
clearCartButton.addEventListener("click", () => {
  cart.length = 0;
  console.log(cart);
  cartContainer.innerHTML = "";
  card();
});

let cartContainer = document.querySelector(".cart-item-container");
let count = document.querySelector("#count");

function card() {
  cartContainer.innerHTML = "";
  let nbrArticles = 0;
  cart.forEach((el) => {
    nbrArticles = cart.length;
    let newDiv = document.createElement("div");
    newDiv.classList.add("cart-item");
    newDiv.setAttribute("id", `${el.id}`);
    newDiv.innerHTML = `
    <div class="cart-img-item">
      <img src="${el.image}" alt="" class="cart-product-img">
    </div>
    <div class="cart-item-info">
      <p class="cart-name-item">${el.name}</p>
      <span class="cart-price-item">$${el.price}</span>
    </div>
    <div class="quantity">
      <span class="plus"><img src="images/plus.svg" alt="" onclick="increase(${el.id})"></span>
      <span>x${el.quantity}</span>
      <span class="minus"><img src="images/minus.svg" alt="" onclick="decrease(${el.id})"></img></span
    </div>
  `;
    cartContainer.append(newDiv);
  });
  let sum = cart.reduce(
    (accumulator, currentValue) =>
      accumulator + currentValue.price * currentValue.quantity,
    0
  );

  count.innerHTML = nbrArticles;
  let total = document.querySelector(".total");
  total.querySelector("span").textContent = "$" + sum;
  document.querySelector(".c-container .bottom").appendChild(total);
  total.style.fontSize = "20px";
  total.querySelector("span").style.fontWeight = "bold";
}

card();

let cartElement = document.querySelector(".cart");
let style = window.getComputedStyle(cartElement);

let cartLogo = document.querySelector(".cart-logo");
cartLogo.style.cursor = "pointer";


// const comp = (a) => a < 992;

// const display = (width) => {
//   if (comp(width)) {
//     cartLogo.addEventListener("click", () => {
//       style.display == "none"
//         ? (cartElement.style.display = "block")
//         : (cartElement.style.display = "none");
//       console.log("click", width);
//     });
//     window.addEventListener("scroll", function () {
//       let scrollPosition = window.scrollY;
//       const seuilDeScroll = 0.5;
//       if (scrollPosition > seuilDeScroll) {
//         cartElement.style.display = "none";
//       }
//     });
//   } else {
//     cartElement.style.display = "block";
//   }
// };

// for (let i = 0; i < 1; i++) {
//   let y = window.innerWidth;
//   display(y);
//   console.log("Boucle");
// }

// window.addEventListener("resize", () => {
//     let screenWidth =
//       window.innerWidth ||
//       document.documentElement.clientWidth ||
//       document.body.clientWidth;
//     console.log( "Largeur de l'écran : " + screenWidth, comp(screenWidth), typeof screenWidth);
//     display(screenWidth);
// })

